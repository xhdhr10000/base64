#include <stdio.h>
#include <string.h>

const char seq[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encode(char *in, char *out)
{
	int v, b=0;
	while (*in != 0) {
		v = (*in) >> 2;
		*out++ = seq[v];

		v = ((*in++) & 0x3) << 4;
		v |= (*in) >> 4;
		*out++ = seq[v];
		if (*in == 0) {b=2; break;}

		v = ((*in++) & 0xf) << 2;
		v |= (*in) >> 6;
		*out++ = seq[v];
		if (*in == 0) {b=1; break;}

		v = (*in++) & 0x3f;
		*out++ = seq[v];
	}
	for (v=0; v<b; v++) *out++ = '=';
	*out = 0;
}

int dvalue(char x)
{
	if (x>='A' && x<='Z') return x-'A';
	if (x>='a' && x<='z') return x-'a'+26;
	if (x>='0' && x<='9') return x-'0'+52;
	if (x=='+') return 62;
	if (x=='/') return 63;
	if (x=='=') return 0;
	printf("Error value %d", x);
	return x;
}

void decode(char *in, char *out)
{
	int v;
	while (*in != 0) {
		v  = dvalue(*in++) << 18;
		v |= dvalue(*in++) << 12;
		v |= dvalue(*in++) << 6;
		v |= dvalue(*in++);
		*out++ = v >> 16;
		*out++ = (v >> 8) & 0xff;
		*out++ = v & 0xff;
	}
	*out = 0;
}

char input[30000], output[40000];

int main()
{
	gets(input);
	input[strlen(input)+1] = input[strlen(input)+2] = 0;
	encode(input, output);
	printf("%s", output);
	printf("\n");
	decode(output, input);
	printf("%s", input);
	printf("\n");
}